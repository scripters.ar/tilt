int tilt = 7;     // seleccionamos pin para el sensor Tilt
int val  = 0;     // variable para leer estado del Sensor tilt

void setup() {
  Serial.begin(9600);
  pinMode(tilt, INPUT);    // configuramos el sensor tilt como Entrada
}

void loop() {
  val = digitalRead(tilt);
  Serial.println(val);    // Imprimimos el valor del pulsador por Serial
}

