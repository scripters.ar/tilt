# Lectura de sensor de inclinación (TILT)

En este repositorio encontraras información básica sobre como conectar un sensor de inclinación con Arduino y leer información a travez de comunicación serial.

## Materiales

* **Una placa Arduino**
* **Una resistencia de 10k**
* **Sensor inclinacion tilt**
* **Un Protoboard**

## Uso

```
En la carpeta diagrams encontraras la conexion del circuito.
```

```
En la carpeta src encontraras el codigo fuente de ejemplo.
```

```
Una vez compilado el codigo en el IDE de arduino ve a Herramientas --> Monitor Serial
```

Una vez activado el monitor serial nos mostrara información sobre el sensor de inclinación.

## Autor

* **Edermar Dominguez** - [Ederdoski](https://github.com/ederdoski)

## License

This code is open-sourced software licensed under the [MIT license.](https://opensource.org/licenses/MIT)
